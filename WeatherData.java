import java.util.ArrayList;

// public class WeatherData {
//     // instance variable declarations
//     public void measurementsChanged() {
//         float temp = getTemperature();
//         float humidity = getHumidity();
//         float pressure = getPressure();
//         currentConditionsDisplay.update(temp, humidity, pressure);
//         statisticsDisplay.update(temp, humidity, pressure);
//         forecastDisplay.update(temp, humidity, pressure);
//     }
//     // other WeatherData methods here
// }

public class WeatherData implements Subject {
    private ArrayList<Observer> observers;
    private float temperature;
    private float humidity;
    private float pressure;

    public WeatherData() {
        observers = new ArrayList<>();
    }

    public void registerObserver(Observer o) {
        observers.add(o);
    }

    public void removeObserver(Observer o) {
        int i = observers.indexOf(o);
        if (i >= 0) {
            observers.remove(i);
        }
    }

    public void notifyObservers() {
        for (int i = 0; i < observers.size(); i++) {
            Observer observer = (Observer) observers.get(i);
            observer.update(temperature, humidity, pressure);
        }
    }

    public void measurementsChanged() {
        notifyObservers();
    }

    public void setMeasurements(float temperature, float humidity, float pressure) {
        this.temperature = temperature;
        this.humidity = humidity;
        this.pressure = pressure;
        measurementsChanged();
    }
    // other WeatherData methods here
}